const person = {
  name: 'Max',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  greet: function() {
    alert('Hi there!');
  }
};

person.greet();

// person.age = 31;
delete person.age;
// person.age = undefined;
// person.age = null;
person.isAdmin = true;

console.log(person);
/*
greet: ƒ ()
hobbies: (2) ["Sports", "Cooking"]
isAdmin: true
name: "Max"
*/