let name = 'Max'

function greet() {
  let age = 30
  //let name = 'Manuel'; // this shadows name, and will work in function
  console.log(name, age);
}

greet(); // Max 30, or Manuel 30 if we use statement above

console.log(age); // will not work

let name = 'Steve' // throws an error, name is already defined

 // Changing everything to var, now everything is global

var name = 'Max'

function greet() {
  var age = 30
  console.log(name, age);
}

greet(); // Max 30

console.log(age); // 30

var name = 'Steve' // no error

// hoisting

console.log(userName);

var userName = 'Max' // gives undefined with var; let or const would produce error
