# JavaScript - The Complete Guide

---

These are my course notes for [JavaScript - The Complete Guide](https://www.udemy.com/course/javascript-the-complete-guide-2020-beginner-advanced/) on [Udemy](www.udemy.com).

- I have taken several courses from Maximilian Schwarzm�ller and I highly recommend them.
- I already had some JavaScript experience before starting this course, so my notes may be pretty sparse in the early sections.
- These are the notes I took for personal use as I needed. They are not polished and may be difficult to follow at times, but I post my notes online for a couple of reasons:
  - to keep myself motivated and hold myself accountable
  - in hopes that it may help someone else taking the same course

---
