/*
1: Create an array of numbers (of your choice) and perform three array operations on it: filter for numbers greater than 5, map every number to an object which holds the number on some property (e.g. "num") and reduce the array to a single number (the multiplication of all numbers).
*/
const myArray = [3, 15, 38, 44]

const filteredArray = myArray.filter(p => p > 5)
console.log("filteredArray: ", filteredArray) 
// [15, 38, 44]

const mappedArray = myArray.map(obj => ({ "num": obj}))
console.log("mappedArray: ", mappedArray) 
/* [{num: 3}
1: {num: 15}
2: {num: 38}
3: {num: 44}]
*/

const reducedArray = myArray.reduce((x, y) => x * y)
console.log("reducedArray: ", reducedArray)
// 75240

//////////////////////////////////////////////////////

/*
2: Write a function ("findMax") which executes some logic that finds the largest number in a list of arguments. Pass the array from task 1 split up into multiple arguments to that function.
*/
findMax = (...findMaxArray) => {
  let maxNum = 0;
  for (n of findMaxArray) {
    if (n > maxNum) {
      maxNum = n;
    }
  }
  return maxNum
}

console.log(findMax(...myArray)) // 44

//////////////////////////////////////////////////////

/*
3: Tweak the "findMax" function such that it finds both the minimum and maximum and returns those as an array. Then use destructuring when calling the function to store the two results in separate constants.
*/
findMax2 = (...findMaxArray) => {
  let maxNum = findMaxArray[0];
  let minNum = maxNum;
  for (n of findMaxArray) {
    if (n > maxNum) {
      maxNum = n;
    } else {
      minNum = n;
    }
  }
  return [minNum, maxNum]
}

const [min, max] = findMax2(...myArray)

console.log(findMax2(...myArray)) // [3, 44]
console.log(min, max) // 3, 44

//////////////////////////////////////////////////////
/*
4: Create a list (and possibly some surrounding logic) where you ensure that NO duplicate values can be added. Use whichever approach seems appropriate to you.
*/

const mySet = new Set();
mySet.add(5);
mySet.add(7);
mySet.add(5);
console.log(mySet); // {5, 7}
