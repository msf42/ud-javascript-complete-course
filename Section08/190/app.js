// concat
const testResults = [1, 5.3, 1.5, 10.99, -5, 10];
const storedResults = testResults.concat([3.99, 2]);

testResults.push(5.91);

console.log("storedResults: ", storedResults); 
// [1, 5.3, 1.5, 10.99, -5, 10, 3.99, 2]

console.log("testResults: ", testResults);
// [1, 5.3, 1.5, 10.99, -5, 10, 5.91]


// index and indexOf
console.log(testResults.indexOf(1.5)); // 2
// finds first index; can also use `lastIndexof`

const personData = [{ name: 'Max' }, { name: 'Manuel' }];
// indexOf does not work on objects
console.log(personData.indexOf({ name: 'Manuel' })); // -1


// findIndex and find

// executes on every element
const manuel = personData.find((person, index, persons) => {
  return person.name === 'Manuel';
});

manuel.name = 'Anna';

console.log(manuel, personData);

const maxIndex = personData.findIndex((person, idx, persons) => {
  return person.name === 'Max';
});

console.log(maxIndex); // 0


// includes
console.log(testResults.includes(10.99)) // true