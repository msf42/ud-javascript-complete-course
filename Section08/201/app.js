// split and join
const data = 'new york;10.99;2000';

// split returns an array, split by character given
const transformedData = data.split(';');
transformedData[1] = +transformedData[1]; // converts to nums if possible
console.log(transformedData);
/*
0: "new york"
1: 10.99
2: "2000"
*/

const nameFragments = ['Max', 'Schwarz'];
const name = nameFragments.join(' ');
console.log(name); // Max Schwartz


// spread operator
const copiedNameFragments = [...nameFragments];
nameFragments.push('Mr');
console.log(nameFragments, copiedNameFragments);
/*
["Max", "Schwarz", "Mr"]
["Max", "Schwarz"]
*/

const prices = [10.99, 5.99, 3.99, 6.59];
console.log(Math.min(...prices)); // 3.99

const persons = [{ name: 'Max', age: 30 }, { name: 'Manuel', age: 31 }];
const copiedPersons = persons.map(person => ({
  name: person.name,
  age: person.age
}));

persons.push({ name: 'Anna', age: 29 });
persons[0].age = 31;

console.log(persons, copiedPersons);
/*
persons:
0: {name: "Max", age: 31}
1: {name: "Manuel", age: 31}
2: {name: "Anna", age: 29}

copiedPersons:
0: {name: "Max", age: 30}
1: {name: "Manuel", age: 31}