// forEach

const prices = [10.99, 5.99, 3.99, 6.59];
const tax = 0.19;
const taxAdjustedPrices = [];

// using a for-of loop
/*
for (const price of prices) {
  taxAdjustedPrices.push(price * (1 + tax));
}
*/

// using forEach
// up to 3 args: value to be used for each iteration, index, full array
prices.forEach((price, idx, prices) => {
  const priceObj = { index: idx, taxAdjPrice: price * (1 + tax) };
  taxAdjustedPrices.push(priceObj);
});

console.log(taxAdjustedPrices);
/*
0: {index: 0, taxAdjPrice: 13.0781}
1: {index: 1, taxAdjPrice: 7.1281}
2: {index: 2, taxAdjPrice: 4.7481}
3: {index: 3, taxAdjPrice: 7.842099999999999}
*/

// map
// map must return something
const taxAdjustedPricesMap = prices.map((price, idx, prices) => {
  const priceObj = { index: idx, taxAdjPrice: price * (1 + tax) };
  return priceObj;
});

console.log(taxAdjustedPricesMap);
// prices remains unchanged
/*
0: {index: 0, taxAdjPrice: 13.0781}
1: {index: 1, taxAdjPrice: 7.1281}
2: {index: 2, taxAdjPrice: 4.7481}
3: {index: 3, taxAdjPrice: 7.842099999999999}
*/

// sort and reverse
// uses strings by default, but we can put our own function in sort/reverse
// const prices = [10.99, 5.99, 3.99, 6.59];
const sortedPrices = prices.sort((a, b) => {
  if (a > b) {
    return -1;
  } else if (a === b) {
    return 0;
  } else {
    return 1;
  }
});
console.log(sortedPrices);
// [10.99, 6.59, 5.99, 3.99]
console.log(sortedPrices.reverse());
// [3.99, 5.99, 6.59, 10.99]


// filter
const filteredArray = prices.filter(p => p > 6);

console.log(filteredArray); // [6.59, 10.99]

// reduce
// reduces an array to a simpler value
const sum = prices.reduce((prevValue, curValue) => prevValue + curValue, 0);

console.log(sum); // 27.56