// Array destructuring
const nameData = ["Max", "Schwarz", "Mr", 30];
// const firstName = nameData[0];
// const lastName = nameData[1];

const [firstName, lastName, ...otherInformation] = nameData;
console.log("firstName = ", firstName); // firstName = Max
console.log("lastName = ", lastName); // lastName = Schwarz
console.log("otherInformation = ", otherInformation); // otherInformation = ["Mr", 30]

// sets
const ids = new Set(["Hi", "from", "set!"]);
ids.add(2); // adds 2 to the set
ids.delete("Hi"); // removes 'Hi'
ids.delete("Hello") // does nothing; no error

// .entries is a set method
// use [0],because it will otherwise return them twice
// can use values() instead
for (const entry of ids.entries()) {
  console.log(entry[0]);
}
// from set! 2


// maps
const person1 = { name: "Max" };
const person2 = { name: "Manuel" };

// we want to attach some information to the 'person1' object
// but we don't want to bloat the object above
const personData = new Map([[person1, [{ date: "yesterday", price: 10 }]]]);
// it is an array of arrays; each pair of arrays is a k:v pair

personData.set(person2, [{ date: "two weeks ago", price: 100 }]);

console.log("personData = ", personData);
/*
personData =  
Map(2) {{…} => Array(1), {…} => Array(1)}
[[Entries]]
0: {Object => Array(1)}
key:
name: "Max"
__proto__: Object
value: Array(1)
0: {date: "yesterday", price: 10}
length: 1
__proto__: Array(0)
1: {Object => Array(1)}
key:
name: "Manuel"
__proto__: Object
value: Array(1)
0: {date: "two weeks ago", price: 100}
length: 1
__proto__: Array(0)
size: (...)
__proto__: Map
*/

console.log("personData.get(person1) = ", personData.get(person1));
/*
personData.get(person1) =  
[{…}]
0:
date: "yesterday"
price: 10
*/

// get both keys and values
for (const [key, value] of personData.entries()) {
  console.log("key, value = ", key, value);
}
/*
key, value =  
{name: "Max"}
0: {date: "yesterday", price: 10 }

key, value =  
{name: "Manuel"}
0: {date: "two weeks ago", price: 100 }
*/

// get keys only
for (const key of personData.keys()) {
  console.log("key = ", key);
}
// key = {name: "Max"}
// key = {name: "Manuel"}

// get values only
for (const value of personData.values()) {
  console.log("value = ", value);
}
// value = {date: "yesterday", price: 10}
// value = {date: "two weeksago", price: 100}

console.log("personData.size = ", personData.size);
// personData.size =  2

// WeakSets
let person = {name: 'Max'};
const persons = new WeakSet();
persons.add(person);

// ... some operations
// person = null;

console.log(persons);

// WeakMap
const personDatax = new WeakMap();
personDatax.set(person, 'Extra info!');

// person = null;

console.log(personDatax);
