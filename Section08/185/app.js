// standard way of creating an array - use this!
const numbers = [1, 2, 3];
console.log("numbers = ", numbers);
// numbers =  (3) [1, 2, 3]

// rare way that creates an empty array of length 5
const emptyArray = new Array(5);
console.log("emptyArray = ", emptyArray)
// emptyArray =  (5) [empty × 5]

// works same as brackets
const moreNumbers = Array(5, 2);
console.log("moreNumbers = ", moreNumbers);
// moreNumbers =  (2) [5, 2]

// also works same as brackets
const yetMoreNumbers = Array.of(1, 2);
console.log("yetMoreNumbers = ", yetMoreNumbers);
// yetMoreNumbers =  (2) [1, 2]

// make an array from DOM objects
const listItems = document.querySelectorAll('li');
console.log("listItems = ", listItems);
// listItems =  NodeList(3) [li, li, li]

// make an array from a list
const arrayListItems = Array.from(listItems);
console.log("arrayListItems = ", arrayListItems);
// arrayListItems =  (3) [li, li, li]

// or from a string
const arrayFromString = Array.from("Annie");
console.log("arrayFromString = ", arrayFromString);
// arrayFromString =  (5) ["A", "n", "n", "i", "e"]

// pop, push, shift, unshift
const hobbies = ['Sports', 'Cooking'];
hobbies.push('Reading'); // adds 'Reading' to the end
hobbies.unshift('Coding'); // adds 'Coding' to beginning
const poppedValue = hobbies.pop(); // pops off 'Reading'
hobbies.shift(); // removes 'Coding'
console.log(hobbies); // ['Sports', 'Cooking']
hobbies[1] = 'Coding'; // changes item
// hobbies[5] = 'Reading'; // adds new, rarely used
console.log(hobbies, hobbies[4]);

// splice
hobbies.splice(1, 0, 'Good Food');
// start at index 1, remove 0 items, replace with 'Good food'
console.log(hobbies); // (3) ["Sports", "Good Food", "Coding"]

const removedElements = hobbies.splice(-2, 1);
// start at index -2 (same as 1) and remove 1 item
console.log(hobbies); // (2) ["Sports", Coding"]
// myArray.splice(3) will remove all from index 3 on

// slice
const testResults = [1, 5.3, 1.5, 10.99, -5, 10];
const storedResults = testResults.slice(2); // makes a copy of testResults starting at index 2

testResults.push(5.91);

console.log("storedResults: ", storedResults); // [1.5, 10.99, -5, 10]
console.log("testResults: ", testResults); // [1, 5.3, 1.5, 10.99, -5, 10, 5.91]