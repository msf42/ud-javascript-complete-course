const randomNumber = Math.random(); // produces random number between 0 (including) and 1 (excluding)

// 1
if (randomNumber > 0.7) {
  alert("randomNumber is greater than 0.7", randomNumber)
}

// 2
let familyAges = [3, 15, 38, 44];

for (let i = 0; i < familyAges.length; i++) {
  console.log(familyAges[i])
}

for (age of familyAges) {
  console.log(age)
}

// 3
for (let i = familyAges.length; i >= 0; i--) {
  console.log(familyAges[i])
}

// 4
const randomNumber2 = Math.random();

if (randomNumber > 0.7 && randomNumber2 > 0.7 || (randomNumber < 0.2 || randomNumber2 < 0.2)) {
  alert(`1 = ${randomNumber}) , 2 = ${randomNumber2})`)
}