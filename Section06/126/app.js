const startGameBtn = document.getElementById('start-game-btn');

function startGame() {
  console.log('Game is starting...');
}

// const person = {
//   greet: function greet() {
//     console.log('Hello there!')
//   }
// }

// person.greet(); // Hello there!

startGameBtn.addEventListener('click', startGame);
// addEventListener is a method on the startGameBtn object

console.dir(startGame); // returns object properties
