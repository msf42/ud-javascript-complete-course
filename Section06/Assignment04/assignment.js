function sayHello(name) {
  console.log('Hi ' + name);
}

// sayHello();

// 1 Rewrite as an arrow function

sayHello1 = (name) => {
  console.log('Hi ' + name)
}


/* 2 
Adjust the arrow function you created as part of task 1 to use three
different syntaxes: With two arguments (incl. a phrase that replaces "Hi"),
with no arguments (hard-coded values in function body) and with one returned value (instead of outputting
text inside of the function directly).

sayHello2a = (greeting, name) => {
  console.log(`${greeting} ${name}`)
}

sayHello2b = () => {
  console.log('Hi Steve')
}

sayHello2c = (name) => {
  return name.toUpperCase();
}

/* 3
Add a default argument to the function you created: A fallback value for
the phrase if no value is provided.*/

sayHello3 = (name="Elvis") => {
  console.log('Hi ' + name)
}

/* 4
Add a new function: "checkInput" which takes an unlimited amount of
arguments (unlimited amount of strings) and executes a callback function
if NO argument/ string is an empty string. */

function checkInput(...strings) {
  if (strings.length > 0) {
    for ( i of strings) {
      console.log(i.toUpperCase())
    }
  } else {
    sayHello3()
  }
}