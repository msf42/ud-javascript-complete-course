const defaultResult = 0;
let currentResult = defaultResult;

function add() {
  currentResult = currentResult + userInput.value;
  outputResult(currentResult, ''); // outputResult is defined in vendor.js
}

addBtn.addEventListener('click', add);
// addBtn is defined in vendor.js
// const addBtn = document.getElementById('btn-add');
