const task3Element = document.getElementById('task-3');

// 1
problem1a = () => {
  alert("This is my answer to problem 1")
}

problem1b = (name) => {
  alert(name)
}

// 2
problem1a();
problem1b("steve");

// 3
task3Element.addEventListener('click', problem1a);

// 4
problem4 = (str1, str2, str3) => {
  return str1 + str2 + str3
}

// 5
alert(problem4("I ", "am", " tired."));