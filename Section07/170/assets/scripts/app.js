const addMovieModal = document.getElementById("add-modal")
// could also use document.querySelector("#add-modal")
// or document.body.children[1]

const startAddMovieButton = document.querySelector('header button');

const toggleMovieModal = () => {
  addMovieModal.classList.toggle('visible')
}

startAddMovieButton.addEventListener('click', toggleMovieModal)