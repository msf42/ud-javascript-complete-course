/* the div with movie info */
const addMovieModal = document.getElementById('add-modal');
// const addMovieModal = document.querySelector('#add-modal');
// const addMovieModal = document.body.children[1];

/* the only button in the header is "Add Movie" */
const startAddMovieButton = document.querySelector('header button');
// const startAddMovieButton = document.querySelector('header').lastElementChild;

/* backdrop is an empty div at the top */
const backdrop = document.getElementById('backdrop');
// const backdrop = document.body.firstElementChild;

/* selects the cancel button */
const cancelAddMovieButton = addMovieModal.querySelector('.btn--passive');


const toggleBackdrop = () => {
  backdrop.classList.toggle('visible');
};

/* runs when the add movie button is clicked
  makes modal and backdrop visible*/
const toggleMovieModal = () => { // function() {}
  addMovieModal.classList.toggle('visible');
  toggleBackdrop();
};

/* runs when cancel button is clicked
  hids modal */
const cancelAddMovie = () => {
  toggleMovieModal();
};

/* hides modal when backdrop is clicked */
const backdropClickHandler = () => {
  toggleMovieModal();
};

startAddMovieButton.addEventListener('click', toggleMovieModal);
backdrop.addEventListener('click', toggleMovieModal);
cancelAddMovieButton.addEventListener('click', cancelAddMovie)