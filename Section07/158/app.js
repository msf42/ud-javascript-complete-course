const ul = document.body.firstElementChild.nextElementSibling;

console.log(ul);
/* output
<ul>
  <li class="list-item">Item 1</li>
  <li class="list-item">Item 2</li>
  <li class="list-item">Item 3</li>
</ul>
*/

const firstLi = ul.firstElementChild;

console.log(firstLi);

/* output
<li class="list-item">Item 1</li>
*/