// 1 Select this task (in two different ways at least!) and change the background-color to black, text-color to white.

const item1a = document.getElementById('task-1')
console.log(item1a)
const item1b = document.querySelector('#task-1')
console.log(item1b)

// Change the document title (in <head></head>) to "Assignment - Solved!". Use two different ways for getting access to the <title> element: Via querySelector on document and via querySelector on the certain property you find in document.

const headTitle = document.head.querySelector('Title');
headTitle.textContent = 'Assignment - Solved!';

// Select the <h1> element on this page and change its text to "Assignment - Solved!".

const h1Title = document.querySelector('h1');
h1Title.textContent = 'Assingment - Solved!'